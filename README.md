Established in 1980 by Jahanshah Josh Nazmiyal, our New York showroom has since flourished with an ever-growing collection including the largest international assortment of Kilim. Drawing on instincts and a wealth of experience, Josh personally selects and oversees each piece in our collection with a team that carries the utmost quality and variety of exotic yarns and weaving techniques; ever pioneering and refining our craft.

Website: https://www.rugandkilim.com/
